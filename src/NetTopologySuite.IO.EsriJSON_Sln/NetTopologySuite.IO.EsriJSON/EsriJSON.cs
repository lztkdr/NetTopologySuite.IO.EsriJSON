﻿namespace NetTopologySuite.IO.EsriJSON
{
    /// <summary>
    /// https://developers.arcgis.com/documentation/common-data-types/geometry-objects.htm#esrijson-specification
    /// </summary>
    internal class EsriJSON
    {
        /// <summary>
        /// 空间参数
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public SpatialReference spatialReference { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public bool hasZ { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public bool hasM { get; set; }


        #region Point

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public double x { get; set; }


        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public double y { get; set; }


        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public double z { get; set; }


        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public double m { get; set; }

        #endregion


        #region Multipoint

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<List<double>> points { get; set; }

        #endregion


        #region Polyline

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<List<List<double>>> paths { get; set; }

        #endregion


        #region Polygon

        /// <summary>
        /// 几何图形
        /// </summary>
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<List<List<double>>> rings { get; set; }

        #endregion
    }

    internal class SpatialReference
    {
        public SpatialReference() { }
        public SpatialReference(int srid)
        {
            this.wkid = srid;
        }
        public int wkid { get; set; }
    }
}
