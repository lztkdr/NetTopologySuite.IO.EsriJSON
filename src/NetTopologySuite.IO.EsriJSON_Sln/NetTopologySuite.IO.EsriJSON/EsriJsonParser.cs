﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetTopologySuite.IO.EsriJSON
{
    public class EsriJsonParser
    {
        protected EsriJsonReader reader;

        protected EsriJsonWriter writer;

        public EsriJsonParser()
        {
            writer = new EsriJsonWriter();
            reader = new EsriJsonReader();
        }

        public virtual Geometry FromRead(string esriJson)
        {
            return reader.Read(esriJson);
        }

        public virtual string ToWrite(Geometry geometry)
        {
            return writer.Write(geometry);
        }
    }
}
