﻿namespace NetTopologySuite.IO.EsriJSON
{
    internal static class Extensions
    {
        public static T GetValue<T>(this IDictionary<string, object> @this, string key, T defvalue = default)
        {
            if (@this.TryGetValue(key, out var objVal))
            {
                return (T)Convert.ChangeType(objVal, typeof(T));
            }
            return defvalue;
        }

        public static EsriJSON ToEsriJSON(this string esriJson)
        {
            var JSON = JsonSerializer.Deserialize<EsriJSON>(esriJson, new JsonSerializerOptions()
            {
                NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals,
                MaxDepth = 50,
            });
            return JSON;
        }

        public static string ToEsriJson(this EsriJSON esriJson)
        {
            var JSON = JsonSerializer.Serialize(esriJson,new JsonSerializerOptions()
            {
                NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals,
                MaxDepth = 50,
            });
            return JSON;
        }
    }
}
