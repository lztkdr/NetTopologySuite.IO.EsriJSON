namespace NetTopologySuite.IO.EsriJSON.TestProject
{
    [TestClass]
    public class EsriJsonTest
    {
        public readonly List<string> lstWKT = new List<string>()
        {
            "MULTIPOLYGON (((114.453156086344 23.9276540161289,114.453084465618 23.9276745910046,114.453194139288 23.9278878493004,114.453223375549 23.9279849969494,114.45326609443 23.9279965378238,114.45328181525 23.9279896393799,114.453282436181 23.9279537603613,114.453275764333 23.927899998224,114.453156086344 23.9276540161289)),((114.445106399819 23.8892900155411,114.445428579637 23.8890805273308,114.445282995512 23.8888931158243,114.444960331107 23.8891264528111,114.445106399819 23.8892900155411)))",
            "POLYGON ((111.270220018138 30.4886211624245,111.269896411441 30.4885355018282,111.269839304377 30.4887020640986,111.270172428918 30.4887829657728,111.270220018138 30.4886211624245))",
        };


        [TestInitialize]
        public void Init()
        {

        }

        [TestMethod]
        public void Test1()
        {
            try
            {
                var rdr_wkt = new WKTReader();
                var rdr_esri = new EsriJsonReader();
                var wtr_esri = new EsriJsonWriter();
                for (int j = 0; j < lstWKT.Count; j++)
                {
                    var wkt = lstWKT[j];
                    var geo_wkt = rdr_wkt.Read(wkt);
                    var geo_rings = rdr_esri.Read(wtr_esri.Write(geo_wkt));
                    if (!geo_wkt.Equals(geo_rings))
                    {
                        Assert.IsTrue(false, $"{j} 出现问题！");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false, ex.Message);
            }
        }


        [TestMethod]
        public void Test2()
        {
            try
            {
                var rdr_wkt = new WKTReader();
                var esriPsr = new EsriJsonParser();
                for (int j = 0; j < lstWKT.Count; j++)
                {
                    var wkt = lstWKT[j];
                    var geo_wkt = rdr_wkt.Read(wkt);
                    var geo_rings = esriPsr.FromRead(esriPsr.ToWrite(geo_wkt));
                    if (!geo_wkt.Equals(geo_rings))
                    {
                        Assert.IsTrue(false, $"{j} 出现问题！");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false, ex.Message);
            }
        }
    }
}