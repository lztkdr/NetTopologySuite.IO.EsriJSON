﻿

# NetTopologySuite.IO.EsriJSON

## 项目介绍

一个 Geometry 与 EsriJSON格式数据 互转类库。


## 使用指南

### 1.  安装

``` shell

Install-Package NetTopologySuite.IO.EsriJSON

```
### 2. 使用


``` cs
using NetTopologySuite.IO.EsriJSON;
```

- 使用示例

``` cs
	var rdr_esri = new EsriJsonReader();
	var wtr_esri = new EsriJsonWriter();

	var wkt = "POLYGON ((111.270220018138 30.4886211624245,111.269896411441 30.4885355018282,111.269839304377 30.4887020640986,111.270172428918 30.4887829657728,111.270220018138 30.4886211624245))";
	var geo_wkt = new WKTReader().Read(wkt);

	//转换成 esriJson格式数据
	var json = wtr_esri.Write(geo_wkt);
	//转换成 Geometry 对象
	var geo_esri = rdr_esri.Read(json);

	Console.WriteLine(geo_wkt.Equals(geo_esri));
```

## 贡献代码

`NetTopologySuite.IO.EsriJSON` 遵循 `Apache-2.0` 开源协议，欢迎大家一起完善，提交 `PR` 或 `Issue`。


## 相关参考

NetTopologySuite：https://github.com/NetTopologySuite/NetTopologySuite
NetTopologySuite.IO.GeoJSON：https://github.com/NetTopologySuite/NetTopologySuite.IO.GeoJSON